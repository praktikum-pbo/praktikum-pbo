# 1. Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat.

Pada program yang saya buat terdapat class PayBills yaitu class yang merepresentasikan pembayaran tagihan untuk dua jenis tagihan, yaitu tagihan listrik (Electricity) dan tagihan PDAM. Berikut adalah penjelasan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman pada program tersebut:

1. Class Paybills memiliki 2 sub class yaitu Electricity, dan PDAM.

2. Class Paybills memiliki method dan variabel yang digunakan untuk menyimpan dan mengakses informasi tentang jenis tagihan, jumlah tagihan, biaya, denda, saldo, dan total tagihan.

3. Method setPayBills() digunakan untuk menyimpan nilai jenis tagihan, jumlah tagihan, denda dan biaya dalam variabel-variabel yang sesuai.

4. Method getBillType() digunakan untuk mengambil nilai jenis tagihan.

5. Method getBillAmount() digunakan untuk mengambil nilai jumlah tagihan.

6. Method getCost() digunakan untuk mengambil nilai biaya.

7. Method calculateBills() digunakan untuk menghitung total tagihan berdasarkan biaya, jumlah tagihan, dan denda. Total tagihan dihitung dengan rumus:

   ```
   total = billAmount _ ((cost _ 30) + charge).
   ```

8. Method showCalculateBills() digunakan untuk menampilkan detail tagihan, termasuk jenis tagihan, ID pelanggan, kategori, jumlah tagihan, biaya, denda, dan total tagihan.

9. Method paymentConfirm() digunakan untuk mengkonfirmasi pembayaran. Jika pengguna memilih untuk membayar, program akan memeriksa apakah saldo cukup untuk membayar total tagihan. Jika saldo cukup, program akan mencetak pesan transaksi berhasil dan memperbarui saldo dengan method updateBalancePaybills().

10. Method updateBalancePaybills() digunakan untuk memperbarui saldo dengan mengurangi total tagihan dari saldo saat ini.

11. Class Electricity merupakan klass turunan dari Paybills yang mengimplementasikan pembayaran tagihan listrik. Class ini memiliki variabel dan method yang khusus untuk pembayaran tagihan listrik.

12. Class PDAM juga merupakan class turunan dari Paybills yang mengimplementasikan pembayaran tagihan PDAM. Class ini memiliki variabel dan method yang khusus untuk pembayaran tagihan PDAM.

13. Pada method payment() dalam class Electricity dan PDAM, program akan meminta pengguna untuk memasukkan ID pelanggan, golongan/tagihan, dan jumlah tagihan. Program akan memvalidasi input dan kemudian menggunakan method setPayBills() untuk menyimpan nilai-nilai tersebut.

14. Selanjutnya, program akan menentukan denda berdasarkan jumlah tagihan menggunakan kondisi if-else. Nilai denda akan disimpan menggunakan method setPayBills().

15. Total tagihan dihitung menggunakan method calculateBills() dengan memasukkan biaya, jumlah tagihan, dan denda sebagai argumen.

16. Terakhir, program akan menampilkan detail tagihan menggunakan method showCalculateBills().

# 2. Mampu menjelaskan algoritma dari solusi yang dibuat

Program yang saya buat yakni Aplikasi Dana, yang mana pada program tersebut terdiri dari beberapa class:

- [Class Dana](../src/Dana.java)

  Class Dana adalah class utama yang berisi metode main(). Di dalam metode main(), terdapat objek Menu dan pemanggilan metode dashboardMenu().

- [Class Menu](../src/Menu.java)

  Class Menu berisi method yang digunakan untuk menampilkan menu dan mengelola interaksi dengan pengguna seperti dashboardMenu(), mainMenu(), transferMenu(), paybillsMenu(). Pada Class Menu juga terdapat objek yang menghubungkan dengan class Akun dan Saldo untuk melakukan operasi terkait akun dan terkait saldo.

- [Class Balance](../src/Balance.java)

  Class Balance merepresntasikan saldo pada akun pengguna. Pada class ini terdapat beberapa method seperti balanceCheck(), topUpBalance(), cashOutBalance()

- [Class Transfer](../src/Transfer.java)

  Class Transfer adalah class abstrak yang merepresentasikan operasi transfer. Pada class ini terdapat metode abstrak untuk pemilihan jenis akun, menghitung biaya admin, dan menampilkan detail transfer.

- [Class Mbanking](../src/Transfer.java)

  Class Mbanking merupakan sub class dari Transfer dan merepresentasikan operasi transfer menggunakan mobile banking. Pada class ini mengimplementasikan methode abstrak dan memiliki method tambahan untuk memperbarui saldo setelah transfer yaitu updateBalanceTransfer().

- [Class Ewallet](../src/Transfer.java)

  Class Ewallet merupakan sub class dari Transfer dan merepresentasikan operasi transfer menggunakan mobile banking. Pada class ini mengimplementasikan metode abstrak dan memiliki method tambahan untuk memperbarui saldo setelah transfer yaitu updateBalanceTransfer().

- [Class Paybills](../src/Paybills.java)

  Class Paybills mewakili operasi pembayaran tagihan. Pada class ini terdapat method untuk memilih jenis tagihan, menghitung total biaya, dan memperbarui saldo setelah membayar tagihan.

- [Class Electricity](../src/Paybills.java)

  Class Electricity merupakan klass turunan dari Paybills yang mengimplementasikan pembayaran tagihan listrik. Class ini memiliki variabel dan method yang khusus untuk pembayaran tagihan listrik.

- [Class PDAM](../src/Paybills.java)

  Class PDAM juga merupakan class turunan dari Paybills yang mengimplementasikan pembayaran tagihan PDAM. Class ini memiliki variabel dan method yang khusus untuk pembayaran tagihan PDAM.

# 3. Mampu menjelaskan konsep dasar OOP

Object Oriented Programming atau OOP merupakan suatu cara penulisan program yang menggunakan pendekatan yang berfokus pada objek dan interaksi antar objek. Objek dapat diartikan sebagai representasi dari suatu class. Class merupakan blueprint atau cetakan yang mendefinisikan atribut dan metode yang dimiliki oleh objek-objek yang akan diciptakan dari class tersebut. Setiap objek yang dibuat berdasarkan class tersebut memiliki karakteristik dan perilaku yang sama seperti yang didefinisikan dalam class.

# 4. Mampu mendemonstrasikan penggunaan Encapsulation secara tepat

Berikut salah satu contoh implementasi encapsulation pada class Account :

![](encapsulation.png)

# 5. Mampu mendemonstrasikan penggunaan Abstraction secara tepat

Berikut salah satu contoh implementasi abstraction pada abstract class Transfer :

![](abstraction.png)

# 6. Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat

Berikut salah satu contoh implementasi abstraction pada inheritance dan polymorphism super class Transfer dan sub class Ewallet & sub class Mbanking :

Silahkan klik link [di sini](inheritance-polymorphism.mp4)

# 7. Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

Untuk melihat proses bisnis silahkan klik link [di sini](Proses%20Bisnis.txt)

# 8. Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table

- Class Diagram

  ![](../assets/Class-Diagram.png)

- Use Case Table

| Use Case | Bisa                                                     | Nilai Prioritas |
| -------- | -------------------------------------------------------- | --------------- |
| User     | Register                                                 | 100             |
| User     | Login                                                    | 100             |
| User     | Mengirim Uang                                            | 100             |
| User     | Menerima Uang                                            | 100             |
| User     | Mendapat Voucher Cashback atau Discount                  | 95              |
| User     | Melakukan Pembayaran di Kasir                            | 95              |
| User     | Membayar Tagihan dan Cicilan                             | 95              |
| User     | Membayar Asuransi                                        | 95              |
| User     | Melakukan Top Up Saldo                                   | 85              |
| User     | Melakukan Cash Out Saldo                                 | 85              |
| User     | Melakukan top-up Saldo                                   | 85              |
| User     | Membeli Pulsa dan Paket Data                             | 80              |
| User     | Membeli Tiket Bioskop                                    | 80              |
| User     | Membeli Tiket Kereta                                     | 80              |
| User     | Membayar Parkir                                          | 80              |
| User     | Berdonasi                                                | 80              |
| User     | Membeli Voucher Belanja                                  | 75              |
| User     | Membuat Perencanaan Keuangan                             | 75              |
| User     | Mengelola Bisnis                                         | 75              |
| User     | Membuat Rekening Keluarga                                | 75              |
| User     | Membuat Tabungan e-Emas                                  | 75              |
| User     | Menyimpan Kartu Kredit atau Debit                        | 70              |
| User     | Melakukan Pembayaran Menggunakan Kartu Kredit atau Debit | 70              |
| User     | Logout                                                   | 65              |

# 9. Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video

Berikut Link Video presentasi project UTS [klik di sini](https://youtu.be/N65EQMUmRGfa).

# 10. Inovasi UX

Inovasi UX pada program yang saya buat yakni berbasis CLI. Melalui penggunaan CLI, program ini menyediakan pengalaman pengguna yang lebih cepat dan efisien. Pengguna dapat mengakses dan menjalankan perintah dengan cepat, mengurangi waktu yang dibutuhkan untuk berpindah antara menu atau tampilan yang berbeda.
