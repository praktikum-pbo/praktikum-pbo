import java.util.Scanner;

class Paybills{
    Scanner input = new Scanner(System.in);
    protected String billType;
    protected int category, billAmount, currentBalance, cost, charge, total;

    // Menyimpan Nilai billType, billAmount, cost
    protected void setPayBills(String billType, int billAmount, int cost) {
        this.billType = billType;
        this.billAmount = billAmount;
        this.cost = cost;
    }

    // Menyimpan Nilai charge
    protected void setPayBills(int charge) {
        this.charge = charge;
    }

    // Mengambil Nilai billType
    protected String getBillType() {
        return billType;
    }

    // Mengambil Nilai billAmount
    protected int getbillAmount() {
        return billAmount;
    }

    // Mengambil cost
    protected int getCost(){
        return cost;
    }

    // Menghitung Total Tagihan
    protected int calculateBills(int cost, int billAmount, int charge){
        return total = billAmount * ((cost * 30) + charge);
    }

    // Menampilkan Detail Tagihan 
    public void showCalculateBills(String billType, int customerID, int category, int billAmount, int cost, int charge){
        System.out.println("-------------------------------------------------");
        System.out.println("=== Tagihan " + billType + " ===");
        System.out.println("ID : " + customerID);
        System.out.println("Kategori : " + category);
        System.out.println("Jumlah Tagihan : " + billAmount);
        System.out.println("Biaya : " + cost);
        System.out.println("Denda : " + charge);
        System.out.println("Total Tagihan Anda : " + total);
        paymentConfirm();
    }

    // Mengkonfirmasi Pembayaran
    protected void paymentConfirm(){
        System.out.println("-------------------------------------------------");
        System.out.print("Bayar Sekarang? <ya/tidak>");
        String bayar = input.next();
        if(bayar.equals("ya"))
            if (this.total > this.currentBalance) {
                System.out.println("Saldo Anda Tidak Cukup \nSilahkan Melakukan Top Up Terlebih Dahulu");
            }else{
                System.out.println("=== Transaksi Berhasill ===");
                updateBalancePaybills();
            }
    }

    // Memperbarui Saldo
    public int updateBalancePaybills(){
        return this.currentBalance -= total;
    }
}

class Electricity extends Paybills { 
    String billType = "Listrik";
    int[] electricCategory = {450, 900, 1300};
    int customerID, inputCategory, electricCost, electricBillAmount, electricCharge;

    // Constructor
    Electricity(int balance){
        this.currentBalance = balance;
    }

    // Mengambil Nilai currentBalance
    public int getCurrentBalance() {
        return currentBalance;
    }

    // Method Pembayaran
    public void payment() {
        System.out.println("-------------------------------------------------");
        System.out.print("Masukkan ID : ");
        customerID = input.nextInt();
        System.out.println("== Golongan ==");
        int j = 1;
        for (int i : electricCategory) {
            System.out.println(j + ". " + i);
            j++;
        }
        System.out.print("Masukkan Golongan : ");
        inputCategory = input.nextInt();
        switch (inputCategory) {
            case 1:
                inputCategory = electricCategory[0];
                electricCost = 415;
                break;
            case 2:
                inputCategory = electricCategory[1];
                electricCost = 1352;
                break;
            case 3:
                inputCategory = electricCategory[2];
                electricCost = 1444;
                break;
            default:
                System.out.println("Inputan Anda Tidak Valid");
                payment();
        }
        System.out.print("Masukkan Jumlah Tagihan (perBulan) : ");
        electricBillAmount = input.nextInt();
        setPayBills(billType, electricBillAmount, electricCost);
        // Menentukan Denda Berdasarkan Jumlah Tagihan
        if(billAmount > 1){
            if(inputCategory == 1 || inputCategory == 2) {
                electricCharge = 3000;
            }else
                electricCharge =  5000;
        }else
            electricCharge = 0;
        setPayBills(electricCharge);
        calculateBills(electricCost, electricBillAmount, electricCharge);
        showCalculateBills(billType, customerID, inputCategory, electricBillAmount, electricCost, electricCharge);
    }
}

class PDAM extends Paybills{
    String billType = "PDAM";
    String[] PDAMCategory = {"I", "II", "III A", "III B", "IV A", "IV B", "V"};
    int customerID, PDAMCost, PDAMBillAmount, PDAMCharge;

    // Constructor
    PDAM(int balance){
        this.currentBalance = balance;
    }

    // Mengambil Nilai currentBalance
    public int getCurrentBalance() {
        return currentBalance;
    }

    // Method pembayaran
    public void payment() {
        System.out.print("Masukkan ID : ");
        customerID = input.nextInt();
        System.out.print("== Golongan (kwh)==");
        int j = 1;
        for (String i : PDAMCategory) {
            System.out.println(j + ". " + i);
            j++;
        }
        System.out.print("Masukkan Golongan : ");
        int inputCategory = input.nextInt();
        switch (inputCategory) {
            case 1:
            case 2:
                PDAMCost = 1050;
                break;
            case 3:
                PDAMCost = 3550;
                break;
            case 4:
                PDAMCost = 4900;
                break;
            case 5:
                PDAMCost = 6825;
                break;
            case 6:
                PDAMCost = 12550;
                break;
            case 7:
                PDAMCost = 14650;
                break;
            default:
                System.out.println("Inputan Anda Tidak Valid");
                payment();
        }
        System.out.print("Masukkan Jumlah Tagihan (Bulan) : ");
        PDAMBillAmount = input.nextInt();
        setPayBills(billType, PDAMBillAmount, PDAMCost);
        // Menentukan Menentukan Denda Berdasarkan Jumlah Tagihan 
        if(billAmount > 1){
            if(inputCategory == 1 || inputCategory == 2) {
                PDAMCharge = 10000;
            }else if(inputCategory == 3 || inputCategory == 4){
                PDAMCharge =  15000;
            }else if(inputCategory == 5){
                PDAMCharge =  35000;
            }else if(inputCategory == 6){
                PDAMCharge =  75000;
            }else
                PDAMCharge =  130000;
        }else
            PDAMCharge = 0;
        setPayBills(PDAMCharge);
        calculateBills(PDAMCost, PDAMBillAmount, PDAMCharge );
        showCalculateBills(billType, customerID, inputCategory, PDAMBillAmount, PDAMCost, PDAMCharge );
    }
}