import java.util.ArrayList;

class TransactionHistory {
    private ArrayList<Transaction> transactionList = new ArrayList<>();

    public void addTransaction(Transaction transaction) {
        transactionList.add(transaction);
    }

    public Transaction getTransaction(int index) {
        return transactionList.get(index);
    }

    public void geTransaction(){
        int i = 1;
        for (Transaction thisList : transactionList) {
            System.out.println(i + ". " + thisList);
            i++;
        }
    }

    public int getTotalTransactions() {
        return transactionList.size();
    }

    
}

class Transaction {
    private String transactionType;
    private double amount;
    private String date;

    // constructor, getter, setter
    Transaction(String transactionType, double amount, String date){
        this.transactionType = transactionType;
        this.amount = amount;
        this.date = date;
    }

    public String getTransactionType(){
        return transactionType;
    }

    public double getAmount(){
        return amount;
    }

    public String getDate(){
        return date;
    }
}

// Contoh penggunaan TransactionHistory
public class History {
    public static void main(String[] args) {
        TransactionHistory history = new TransactionHistory();

        // Tambahkan transaksi ke riwayat
        Transaction transaction1 = new Transaction("Transfer", 100.0, "2023-05-17");
        history.addTransaction(transaction1);

        Transaction transaction2 = new Transaction("Pembayaran", 50.0, "2023-05-18");
        history.addTransaction(transaction2);

        // Akses transaksi dari riwayat
        Transaction retrievedTransaction = history.getTransaction(0);
        System.out.println(retrievedTransaction.getTransactionType()); // Output: Transfer

        // Menghitung total transaksi
        int totalTransactions = history.getTotalTransactions();
        System.out.println(totalTransactions); // Output: 2

        history.geTransaction();
    }
}
