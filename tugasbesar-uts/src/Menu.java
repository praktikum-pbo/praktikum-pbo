import java.util.*;

class Menu{
    Scanner input = new Scanner(System.in);
    Account account = new Account("username", 11111);
    Balance balance = new Balance();
    int currentBalance, option = 0;
    String menu;
    boolean login;

    // Menu Dashboard
    public void dashboardMenu(){
        while (option != 3) {
            System.out.println("=================================================");
            System.out.println("=\t    ---- Selamat Datang! ----    \t=");
            System.out.println("=================================================");
            System.out.println("=== Menu Utama ===");
            System.out.println("1. Buat Akun \n2. Login \n3. Keluar");
            System.out.print("Masukkan Pilihan Anda : ");
            option = input.nextInt();
            switch (option) {
                case 1:
                    account.createAccount();
                    break;
                case 2:
                    login = account.accountCheck(true);
                    if(login){
                        mainMenu(); // Memanggil method menu utama setelah user melakukan login
                    }
                    break;
                case 3: 
                System.out.println("=================================================");
                System.out.println("=\t    ---- Terima Kasih! ----    \t\t=");
                System.out.println("=================================================");                
                    break;
                default:
                    System.out.println("Inputan Anda Tidak Valid");
                    break;
            }
        }
    }

    // Menu Utama 
    public void mainMenu(){
        while (option != 7) {
            System.out.println("-------------------------------------------------");
            System.out.println("=== Menu Utama ===");
            System.out.println("1. Transfer \n2. Tagihan \n3. Cek Saldo \n4. Top Up Saldo \n5. Cash Out Saldo  \n6. Profil \n7. Logout");
            System.out.print("Masukkan Pilihan Anda : ");
            option = input.nextInt();
            switch (option) {
                case 1:
                    transferMenu();
                    break;
                case 2:
                    paybillsMenu();
                    break;
                case 3:
                    balance.balanceCheck();
                    break;
                case 4: 
                    balance.topUpBalance(); 
                    this.currentBalance = balance.getBalance();   
                    break;
                case 5: 
                    balance.cashOutBalance();
                    this.currentBalance = balance.getBalance();
                    break;
                case 6:
                    account.showAccount();
                    break;
                case 7:
                    break;
                default:
                    System.out.println("Inputan Anda Tidak Valid");
                    break;
            }
            balance.setBalance(currentBalance); 
        } 
    }

    // Menu Transfer
    public void transferMenu(){
        System.out.println("-------------------------------------------------");
        System.out.println("=== Menu Transfer ===");
        System.out.println("Pilih Jenis Rekening Penerima ");
        System.out.println("1. Mbanking \n2. E-Wallet");
        System.out.print("Masukkan Pilihan Anda : ");
        option = input.nextInt();
        switch (option) {
            case 1:
                Mbanking bank = new Mbanking(currentBalance);
                bank.accountType();
                this.currentBalance = bank.getCurrentBalance();
                break;
            case 2:
                Ewallet ewallet = new Ewallet(currentBalance);
                ewallet.accountType();
                this.currentBalance = ewallet.getCurrentBalance();
                break;
            default:
                System.out.println("Inputan Anda Tidak Valid");
                transferMenu();
                return;
        }
    }

    // Menu Pembayaran Tagihan
    public void paybillsMenu(){
        System.out.println("-------------------------------------------------");
        System.out.println("=== Menu Tagihan ===");
        System.out.println("Pilih Jenis Tagihan");
        System.out.println("1. Listrik \n2. PDAM");
        System.out.print("Masukkan Pilihan Anda : ");
        option = input.nextInt();
        switch (option) {
            case 1:
                Electricity electricity = new Electricity(currentBalance);
                electricity.payment();
                this.currentBalance = electricity.getCurrentBalance();
                break;
            case 2:
                PDAM pdam = new PDAM(currentBalance);
                pdam.payment();
                this.currentBalance = pdam.getCurrentBalance();
                break;
            default:
                System.out.println("Inputan Anda Tidak Valid");
                paybillsMenu();
        }
    }
}