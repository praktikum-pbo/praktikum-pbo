import java.util.Scanner;

class Balance{
    Scanner input = new Scanner(System.in);
    int balance;

    //Cek Saldo
    public void balanceCheck(){
        System.out.println("Saldo Anda Saat Ini : " + getBalance());
    }

    // Menyimpan Nilai Saldo
    public void setBalance(int balance) {
        this.balance = balance;
    }

    // Mengambil Nilai Saldo
    public int getBalance() {
        return balance;
    }


    // Top Up Saldo
    protected void topUpBalance(){        
        System.out.println("-------------------------------------------------");
        System.out.print("Masukkan Jumlah Uang yang Ingin Anda Simpan : ");
        int inputBalance = input.nextInt();
        int newBalance = this.balance + inputBalance;
        setBalance(newBalance);
        System.out.println("Transaksi Anda Berhasil");
        System.out.println("Saldo yang Anda Masukkan Sejumlah " + inputBalance);
        balanceCheck();
    }

    // Cash Out Saldo
    protected void cashOutBalance(){
        System.out.println("-------------------------------------------------");
        System.out.print("Masukkan Jumlah Uang yang Ingin Anda Ambil : ");
        int amount = input.nextInt();
        // Validasi Ketersediaan Saldo
        if(amount < this.balance){
            this.balance -= amount;
            System.out.println("-------------------------------------------------");
            System.out.println("=== Transaksi Anda Berhasil ===");
            System.out.println("Saldo yang Anda Ambil Sejumlah " + amount);
            balanceCheck();
        }else{
            System.out.println("-------------------------------------------------");
            System.out.println("Saldo Anda Kurang!");
            System.out.println("Transaksi Anda Gagal");
            cashOutBalance();
        }
    }
}