import java.util.Date;


public class TesWaktu {
    public static void main(String[] args) {
        var date1 = new Date();
        var date2 = new Date(System.currentTimeMillis());

        System.out.println(date1);
        System.out.println(date2);

    }
}
