import java.util.Scanner;
import java.util.Date;

abstract class Transfer {
    protected Scanner input = new Scanner(System.in);
    public Menu menu = new Menu();
    protected String accountType;
    protected int accountNumber, option, nominal, adminFee, total;

    // Abstract Method
    public abstract void accountType();
    protected abstract int adminFee();
    public abstract void transferDetail();
    
    // Menampilkan Detail Transfer
    protected void showTransferDetail(int accountNumber, int nominal, int adminFee, int total){
        System.out.println("-------------------------------------------------");
        System.out.println("=== Transfer Berhasil ===");
        System.out.println("Rekening Tujuan : " + accountNumber);
        System.out.println("Jumlah Transfer : " + nominal);
        System.out.println("Biaya Admin : " + adminFee);
        System.out.println("Total Transfer : " + total);
        var date = new Date();
        System.out.println("Tanggal : " + date);
    }
}

class Mbanking extends Transfer{
    private String[] mBanking = {"BNI Mobile", "BCA Mobile", "BRImo"};
    public int currentBalance;

    // Constructor
    Mbanking(int balance){
        this.currentBalance = balance;
    }

    // Mengambil Nilai currentBalance
    public int getCurrentBalance() {
        return currentBalance;
    }

    // Pemilihan Jenis Rekening Tujuan 
    public void accountType(){
        System.out.println("-------------------------------------------------");
        System.out.println("=== M-Banking ===");
        int j = 1;
        for (String i : this.mBanking) {
            System.out.println(j + ". " + i);
            j++;
        }
        System.out.print("Masukkan pilihan anda : ");
        option = input.nextInt();
        if(option >= 1 && option <= mBanking.length){
            accountType = mBanking[option-1];
        }else{
            System.out.println("Inputan Anda Salah");
        }
        transferDetail();
    }

    // Biaya Admin
    protected int adminFee(){
        if(nominal >= 50000) return adminFee = 0;
        return adminFee = 5000;
    }

    // Transfer Detail
    public void transferDetail(){
        System.out.println("-------------------------------------------------");
        System.out.print("Masukkan Nomor Rekening Penerima : ");
        accountNumber = input.nextInt();
        System.out.print("Masukkan Nominal yang ingin ditransfer : ");
        nominal = input.nextInt();
        adminFee = adminFee();
        total = nominal + adminFee;
        if(total > currentBalance){
            System.out.println("Saldo Anda Tidak Cukup \nSilahkan Melakukan Top Up Terlebih Dahulu");
        }else{
            showTransferDetail(accountNumber, nominal, adminFee, total);
            updateBalanceTransfer();
        }
    }

    // Memperbarui Saldo
    public int updateBalanceTransfer(){
        return this.currentBalance -= this.total;
    }
}

class Ewallet extends Transfer{
    private String[] eWalllet = {"Dana", "Shopeepay", "Ovo"};
    public int currentBalance; 

    // Constructor
    Ewallet(int balance){
        this.currentBalance = balance;
    }

    // Mengambil Nilai currentBalance
    public int getCurrentBalance() {
        return currentBalance;
    }

    // Pemilihan Jenis Rekening Tujuan 
    public void accountType(){
        System.out.println("-------------------------------------------------");
        System.out.println("=== E-Wallet ===");
        int j = 1;
        for (String i : this.eWalllet) {
            System.out.println(j + ". " + i);
            j++;
        }
        System.out.print("Masukkan pilihan anda : ");
        option = input.nextInt();
        if(option >= 1 && option <= eWalllet.length){
            accountType = eWalllet[option-1];
        }else{
            System.out.println("Inputan Anda Salah");
        }
        transferDetail();
    }

    // Biaya Admin
    protected int adminFee(){
        if(nominal >= 50000) return adminFee = 0;
            if(option == 1) return adminFee = 0;
            return adminFee = 1000;
    }

    // Transfer Detail
    public void transferDetail(){
        System.out.println("-------------------------------------------------");
        System.out.print("Masukkan Nomor Rekening Penerima : ");
        accountNumber = input.nextInt();
        System.out.print("Masukkan Nominal yang ingin ditransfer : ");
        nominal = input.nextInt();
        adminFee = adminFee();
        total = nominal + adminFee;
        if(total > currentBalance){
            System.out.println("Saldo Anda Tidak Cukup \nSilahkan Melakukan Top Up Terlebih Dahulu");
        }else{
            showTransferDetail(accountNumber, nominal, adminFee, total);
            updateBalanceTransfer();
        }
    }

    // Memperbarui Saldo
    public int updateBalanceTransfer(){
        return this.currentBalance -= this.total;
    }
}