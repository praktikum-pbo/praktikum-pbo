import java.util.*;

class Account {
    public String phoneNumber, name, email, confirm;
    private int inputPin, pin, newPin, confirmPin;
    Scanner input = new Scanner(System.in);

    // constructor
    Account(String phoneNumber, int pin){
        this.phoneNumber = phoneNumber;
        this.pin = pin;
    }

    // Method untuk menyimpan nomor hp dan pin user
    protected void setAccount(String phoneNumber, int pin){
        this.phoneNumber = phoneNumber;
        this.pin = pin;
    }

    // Method untuk menyimpan nama dan email user
    protected void setAccount(String name, String email){
        this.name = name;
        this.email = email;
    }

    // Method untuk menyimpan pin baru
    protected void setAccount(int pin){
        this.pin = pin;
    }

    // Buat Akun
    public void createAccount(){
        System.out.println("-------------------------------------------------");
        System.out.println("=== Buat Akun ===");
        System.out.print("Masukkan No. Hp Anda : ");
        String inputphoneNumber = input.next();
        System.out.print("Masukkan Pin Anda : ");
        inputPin = input.nextInt();
        setAccount(inputphoneNumber, inputPin);
    }

    // Login
    public boolean accountCheck(boolean verifAkun){
        System.out.println("-------------------------------------------------");
        System.out.println("=== Masuk ===");
        System.out.print("Masukkan No. Hp Anda : ");
        String inputPhoneNumber = input.next();
        System.out.print("Masukkan Pin Anda : ");
        inputPin = input.nextInt();
        // Validasi Akun
        if(this.phoneNumber.equals(inputPhoneNumber) && inputPin == this.pin){
            System.out.println("Selamat Anda Berhasil Login sebagai " + this.phoneNumber); 
            accountInfo();
            verifAkun = true; 
        }else{
            System.out.println("No. Hp atau Password yang Anda Masukkan Salah!");
            verifAkun = false;
        }
        return verifAkun;
    }

    // Ubah Pin
    protected void updatePin(){
        System.out.println("=== Ganti Pin ===");
        System.out.print("Masukkan Pin Lama : ");
        inputPin = input.nextInt();
        System.out.print("Masukkan Pin Baru : ");
        newPin = input.nextInt();
        System.out.print("Konfirmasi Pin baru : ");
        confirmPin = input.nextInt();
        if(newPin == confirmPin && inputPin == this.pin){
            setAccount(confirmPin);
            System.out.println("Pin Anda Berhasil Diubah!");
        }else{
            System.out.println("Pin Salah atau Tidak Cocok");
            System.out.print("Ingin Mencoba Mengganti Pin Lagi? <ya/tidak>"); 
            confirm = input.next();
            if(confirm.equals("ya")){
                updatePin();
            }
        }
    }

    // Informasi Akun
    public void accountInfo(){
        System.out.println("-------------------------------------------------");
        System.out.println("=== Profil ===");
        System.out.println("Silahkan Lengkapi Profil Anda!");
        Scanner input2 = new Scanner(System.in);
        System.out.print("Masukkan Nama Lengkap Anda : ");
        String inputName = input2.nextLine(); 
        System.out.print("Masukkan Email Anda : ");
        String inputEmail = input2.nextLine();
        setAccount(inputName, inputEmail);
    }

    // Menampilkan Akun
    public void showAccount(){
        System.out.println("-------------------------------------------------");
        System.out.println("=== Profil ===");
        System.out.println("No. Hp \t\t: " + phoneNumber);
        System.out.println("Nama \t\t: " + this.name);
        System.out.println("Email \t\t: " + email);
        System.out.println("-------------------------------------------------");
        System.out.print("Ingin Mengubah Pin? <ya/tidak>");
        confirm = input.next();
        if(confirm.equals("ya")){
            updatePin();
        }
    }
}