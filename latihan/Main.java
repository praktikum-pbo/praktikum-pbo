import java.util.Scanner;

class Warga{
    Scanner input = new Scanner(System.in);
    int Penghasilan = 1000;
    int Skills = 1;
    String Motivasi = "ya";

    void diberiPelatihan(){
        tampilkan();
        System.out.print("Beri motivasi juga? <ya/tidak>");
        String userInput = input.nextLine();
        if(userInput.equals(Motivasi)){
            diberiMotivasi();
        }else{
            System.out.println("Skills warga tidak bertambah!" + "\nButuh Motivasi juga!");
        }
    }

    void diberiMotivasi(){
        this.Skills += 99999999;
        tampilkan();
        System.out.println("Kerennn! Berkat Pelatihan dan Motivasi dari kamu, Skills warga menjadi meningkat!");
    }

    void tampilkan(){
        System.out.println("Penghasilan : " + this.Penghasilan);
        System.out.println("Skills : " + this.Skills);
    }
}

public class Main{
    public static void main(String[] args) {
        Warga warga = new Warga();
        warga.diberiPelatihan();
    }
}