# 1. Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:

- User
  | Use Case | Bisa | Nilai Prioritas |
  | -------- | ---------------------------- | --------------- |
  | User | Register | 100 |
  | User | Login | 100 |
  | User | Mengirim Uang | 100 |
  | User | Menerima Uang | 100 |
  | User | Mendapat Voucher Cashback atau Discount | 95 |
  | User | Melakukan Pembayaran di Kasir | 95 |
  | User | Membayar Tagihan dan Cicilan | 95 |
  | User | Membayar Asuransi | 95 |
  | User | Melakukan Top Up Saldo | 85 |
  | User | Melakukan Cash Out Saldo | 85 |
  | User | Melakukan top-up Saldo | 85 |
  | User | Membeli Pulsa dan Paket Data | 80 |
  | User | Membeli Tiket Bioskop | 80 |
  | User | Membeli Tiket Kereta | 80 |
  | User | Membayar Parkir | 80 |
  | User | Berdonasi | 80 |
  | User | Membeli Voucher Belanja | 75 |
  | User | Membuat Perencanaan Keuangan | 75 |
  | User | Mengelola Bisnis | 75 |
  | User | Membuat Rekening Keluarga | 75 |
  | User | Membuat Tabungan e-Emas | 75 |
  | User | Menyimpan Kartu Kredit atau Debit | 70 |
  | User | Melakukan Pembayaran Menggunakan Kartu Kredit atau Debit | 70 |
  | User | Logout | 65 |

- Manajemen Perusahaan
  | Use Case | Bisa | Nilai Prioritas |
  | -------- | ---------------------------- | --------------- |
  | Manajemen | Mengelola Akun Perusahaan | 95 |
  | Manajemen | Mengelola Keuangan Perusahaan | 95 |
  | Manajemen | Mengelola Pelaporan Keuangan | 90 |
  | Manajemen | Memonitor Transaksi dan Penggunaan Dana| 85 |
  | Manajemen | Mengelola Data Karyawan dan Proses Pembayaran Gaji Karyawan | 85 |
  | Manajemen | Melakukan Analisis Keuangan | 85 |
  | Manajemen | Mengelola Proyek dan Pengeluaran Anggaran | 80 |
  | Manajemen | Melakukan Audit Keuangan | 75 |  
  | Manajemen | Menyediakan Layanan Pelanggan | 75 |
  | Manajemen | Mengelola Faktur dan Tagihan| 70 |

- Direksi Perusahaan

  | Use Case | Bisa                                                                      | Nilai Prioritas |
  | -------- | ------------------------------------------------------------------------- | --------------- |
  | Direksi  | Melihat Informasi Terkini Kinerja Bisnis Perusahaan                       | 95              |
  | Direksi  | Melakukan Analisis Data Keuangan Perusahaan                               | 95              |
  | Direksi  | Melihat Informasi Data Pasar, Analisis Pesaing, dan Perkiraan Tren Bisnis | 95              |
  | Direksi  | Melihat Tren Transaksi dan Penggunaan Dana Perusahaan                     | 90              |
  | Direksi  | Memiliki Akses Informasi Khusus Direksi                                   | 90              |
  | Direksi  | Memonitor Kemajuan Proyek strategis                                       | 90              |
  | Direksi  | Memiliki Akses Informasi Khusus Direksi                                   | 90              |
  | Direksi  | Melakukan Analisis dan Kepatuhan Bisnis                                   | 85              |
  | Direksi  | Memantau Performa Karyawan                                                | 85              |
  | Direksi  | Mengelola Pelaporan Keuangan                                              | 80              |
  | Direksi  | Melakukan Kustomisasi Tampilan Dashboard                                  | 80              |

# 2. Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital

Berikut Class Diagram pada aplikasi Dana

![](Class-Diagram.png)

# 3. Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

SOLID Design Principle adalah kumpulan prinsip desain perangkat lunak yang membantu meningkatkan kualitas dan fleksibilitas aplikasi. Implementasi SOLID pada aplikasi Dana Dompet Digital meliputi:

- S : Setiap kelas memiliki satu tanggung jawab, misalnya kelas transaksi hanya menangani transaksi.
- O : Aplikasi mudah diperluas tanpa merubah kode yang sudah ada, contohnya menambahkan metode pembayaran baru.
- L : Kelas turunan dapat menggantikan kelas induk tanpa mengubah perilaku aplikasi.
- I : Antarmuka dibagi sesuai kebutuhan kelas, mencegah kelas mengimplementasikan metode yang tidak relevan.
- D : Kode tidak bergantung langsung pada detail implementasi, melainkan menggunakan abstraksi dan injeksi ketergantungan.

# 4. Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih

Pada Project ini saya menggunakan Design Pattern MVC, yaitu memisahkan folder model, view, dan controller.

![](MVC.png)

# 5. Mampu menunjukkan dan menjelaskan konektivitas ke database

Alur konektivitas database adalah sebagai berikut:

1. Konfigurasi koneksi ke database MySQL terdapat dalam file .env dengan detail host "localhost", port "3306", username "root", password kosong, dan nama database "uaspbo".
2. Migrasi "api_tokens" digunakan untuk membuat tabel "api_tokens" dengan kolom-kolom seperti "id", "user_id" (foreign key ke tabel "users"), "name", "type", "token" (nilai token yang unik), dan "expires_at" (batas waktu berlakunya token, bisa kosong).
3. Migrasi "users" digunakan untuk membuat tabel "users" dengan kolom-kolom seperti "id", "email", "password" (kata sandi yang di-hash), "nohp" (nomor telepon unik), "name", "saldo" (nilai saldo, default 0), dan "remember_me_token" (token untuk mengingat sesi login, bisa kosong).
4. Model "User.ts" adalah representasi model entitas "User" dengan properti sesuai dengan kolom-kolom dalam tabel "users". Model ini juga memiliki metode hashPassword untuk meng-hash kata sandi sebelum disimpan di database.
5. Model "ApiToken.ts" adalah representasi model entitas "ApiToken" dengan properti sesuai dengan kolom-kolom dalam tabel "api_tokens".

![](Konektivitas-Databse.png)

![](modeling.png)

# 6. Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya

- Register

Alur register di atas adalah sebagai berikut:

1. Klien mengirimkan permintaan POST ke "/api/v1/register" dengan data pengguna baru.
2. Server memvalidasi data input dari klien.
3. Jika data valid, server membuat entitas "User" baru dan menyimpannya di database.
4. Server mengirimkan respon dengan pesan "register berhasil" jika berhasil, atau pesan kesalahan jika ada masalah.

- Method Register
  ![](method-register.png)
- Pengujian di postman
  ![](Register.png)

- Login

Alur login di atas adalah sebagai berikut:

1. Klien mengirimkan permintaan POST ke "/api/v1/login" dengan data nomor telepon (nohp) dan kata sandi (password) pengguna yang ingin login.
2. Server memvalidasi data input dari klien untuk memastikan kedua data tersebut ada dan sesuai formatnya.
3. Jika data valid, server mencoba untuk melakukan otentikasi dengan menggunakan AdonisJS Auth, yaitu memeriksa apakah kombinasi nomor telepon dan kata sandi sesuai dengan data yang ada di database.
4. Jika otentikasi berhasil, server mengirimkan respon dengan status 200 OK dan menyertakan pesan "Login Berhasil" serta token akses yang akan digunakan untuk otorisasi akses selanjutnya.
5. Jika otentikasi gagal, server mengirimkan respon dengan status 401 Unauthorized dan menyertakan pesan "Login Gagal".

- Method Login
  ![](method-login.png)
- Pengujian di postman
  ![](Login.png)

- Logout

Alur logout di atas adalah sebagai berikut:

1. Klien mengirimkan permintaan POST ke "/api/v1/logout" dengan data token akses yang ingin digunakan untuk melakukan logout.
2. Server menggunakan token akses tersebut untuk mengidentifikasi pengguna yang ingin logout.
3. Jika pengguna teridentifikasi (token akses valid), server mencari dan menghapus token akses dari database untuk pengguna tersebut. Hal ini bertujuan untuk mencabut token akses sehingga tidak bisa lagi digunakan untuk mengakses fitur yang memerlukan otorisasi.
4. Selanjutnya, server melakukan revoke (mencabut) seluruh token akses yang terkait dengan pengguna tersebut menggunakan AdonisJS Auth.
5. Jika seluruh proses berjalan dengan sukses, server mengirimkan respon dengan status 200 OK dan pesan "Logout Berhasil" yang menandakan bahwa pengguna telah berhasil logout.

- Method Logout
  ![](Logout.png)
- Pengujian di postman
  ![](method-logout.png)

- Route
  ![](route.png)

# 7. Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital

Pada Program ini saya menggunakan Angular dengan bahasa pemrograman typescript untuk User Interface nya.
![](Angular.png)

# 8. Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

# 9. Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

# 10. Bonus: Mendemonstrasikan penggunaan Machine Learning
