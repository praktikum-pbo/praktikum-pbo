import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import { schema, rules } from "@ioc:Adonis/Core/Validator";
import ApiToken from "App/Models/ApiToken";
import User from "App/Models/User";

export default class AuthController {
  public async register({ request, response }: HttpContextContract) {
    try {
      const registerValidation = schema.create({
        nohp: schema.string({}, [
          rules.unique({
            table: "users",
            column: "nohp",
          }),
        ]),
        password: schema.string({}, [rules.minLength(6)]),
        name: schema.string(),
        email: schema.string({}, [rules.email()]),
      });
      const payload = await request.validate({ schema: registerValidation });
      await User.create(payload);

      return response.created({
        messsage: "register berhasil",
      });
    } catch (error) {
      return response.badRequest({
        massage: error.message,
      });
    }
  }

  public async login({ request, response, auth }: HttpContextContract) {
    try {
      const loginValidation = schema.create({
        nohp: schema.string(),
        password: schema.string(),
      });
      await request.validate({ schema: loginValidation });

      const nohp = request.input("nohp");
      const password = request.input("password");

      const token = await auth.use("api").attempt(nohp, password, {
        expiresIn: "1 Days",
      });

      return response.ok({
        message: "Login Berhasil",
        token,
      });
    } catch (error) {
      console.error("Error:", error);
      return response.unauthorized({
        message: "Login Gagal",
      });
    }
  }

  public async logout({ response, auth }: HttpContextContract) {
    try {
      const user = auth.user;

      if (user) {
        const apiToken = await ApiToken.query()
          .where("userId", user.id)
          .first();

        if (apiToken) {
          await apiToken.delete();
        }
      }

      await auth.use("api").revoke();

      return response.ok({
        message: "Logout Berhasil",
      });
    } catch (error) {
      console.error("Error:", error);
      return response.badRequest({
        message: "Logout Gagal",
      });
    }
  }
}
