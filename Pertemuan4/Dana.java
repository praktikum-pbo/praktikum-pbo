package Pertemuan4;
import java.util.*;

public class Dana {
    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.thisMenu();
    }
}

class Menu{
    Scanner input = new Scanner(System.in);
    int pil;
    public void thisMenu(){
        System.out.println("Pilih Jenis Rekening Penerima ");
        System.out.println("1. Mbanking \n2. E-Wallet");
        System.out.print("Masukkan Pilihan Anda : ");
        pil = input.nextInt();
        switch (pil) {
            case 1:
                Mbanking bank = new Mbanking();
                bank.jenisRek();
                break;
            case 2:
                Ewallet wallet = new Ewallet();
                wallet.jenisRek();
                break;
            default:
                System.out.println("Inputan anda tidak valid");
        }
    }
}
