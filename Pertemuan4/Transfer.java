package Pertemuan4;
import java.util.Scanner;

public abstract class Transfer {
    protected Scanner input = new Scanner(System.in);
    protected Saldo sl = new Saldo();
    protected int NoRek, pil, Nominal, BiayaAdmin, Total; 
    protected String jenisRek;

    public abstract void jenisRek();
    protected abstract int BiayaAdmin();

    protected void inputRek(){
        System.out.print("Masukkan Nomor Rekening Penerima : ");
        NoRek = input.nextInt();
    }

    protected void inputNominal(){
        System.out.print("Masukkan Nominal yang ingin ditransfer : ");
        Nominal = input.nextInt();
    }

    protected void hitungTransfer(){
        BiayaAdmin = BiayaAdmin();
        Total = Nominal + BiayaAdmin;
        System.out.println("Total transfer : " + Total);
        sl.saldo -= Total;
        sl.cekSaldo();
    }

    protected class Saldo{
        protected Scanner input = new Scanner(System.in);
        protected int saldo = 1000000000;

        protected void cekSaldo(){
            System.out.println("Saldo Anda saat ini : " + saldo);
        }
    }
}

class Mbanking extends Transfer{
    private String[] mBanking = {"BNI Mobile", "BCA Mobile", "BRImo"};

    public void jenisRek(){
        System.out.println("M-Banking");
        int j = 1;
        for (String i : this.mBanking) {
            System.out.println(j + ". " + i);
            j++;
        }
        System.out.print("Masukkan pilihan anda : ");
        pil = input.nextInt();
        if(pil >= 1 && pil <= mBanking.length){
            jenisRek = mBanking[pil-1];
        }else{
            System.out.println("Inputan Anda Salah");
        }
        inputRek();
        inputNominal();
        hitungTransfer();
    }

    protected int BiayaAdmin(){
        if(Nominal >= 50000){
            BiayaAdmin = 0;
        }else{
            BiayaAdmin = 5000;
        }
        return BiayaAdmin;
    }
}

class Ewallet extends Transfer{
    private String[] eWalllet = {"Dana", "Shopeepay", "Ovo"};

    public void jenisRek(){
        System.out.println("E-Wallet");
        int j = 1;
        for (String i : this.eWalllet) {
            System.out.println(j + ". " + i);
            j++;
        }
        System.out.print("Masukkan pilihan anda : ");
        pil = input.nextInt();
        if(pil >= 1 && pil <= eWalllet.length){
            jenisRek = eWalllet[pil-1];
        }else{
            System.out.println("Inputan Anda Salah");
        }
        inputRek();
        inputNominal();
        hitungTransfer();
    }

    protected int BiayaAdmin(){
        if(Nominal >= 50000){
            BiayaAdmin = 0;
        }else{
            BiayaAdmin = 5000;
        }
        return BiayaAdmin;
    }
}